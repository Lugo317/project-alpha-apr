from django.urls import path
from projects.views import (
    project_list_view,
    show_project_view,
    create_project_view,
)


urlpatterns = [
    path("", project_list_view, name="list_projects"),
    path("<int:id>/", show_project_view, name="show_project"),
    path("create/", create_project_view, name="create_project"),
]
